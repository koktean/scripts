// Test case for c02ex01
#include <stdio.h>
#include <string.h>

char *ft_strncpy(char *dest, char *src, unsigned int n);

void put_str(char *c, int n)
{
    for(int i = 0; i < n; i++)
    {
        printf("%c", c[i]);
    }
}

int main()
{
    // No null termination check
    char src[] = "abcdefghijklmnopqrst", dest[] = "ABCDEFGHIJKLMNOPQRST";
    char src1[] = "abcdefghijklmnopqrst", dest1[] = "ABCDEFGHIJKLMNOPQRST";
    printf("Expected: ");
    put_str(strncpy(dest, src, 10), 20);
    printf("\nReturns: ");
    put_str(ft_strncpy(dest1, src1, 10), 20);
    printf("\n");
    
    // Null filling check
    char src2[] = "abcde", dest2[] = "ABCDEFGHIJKLMNOPQRST";
    char src3[] = "abcde", dest3[] = "ABCDEFGHIJKLMNOPQRST";
    printf("Expected: ");
    put_str(strncpy(dest2, src2, 10), 20);
    printf("\nReturns: ");
    put_str(ft_strncpy(dest3, src3, 10), 20);
    printf("\n");
    
    return 0;
}
