// Test case for c02ex00
#include <stdio.h>
#include <string.h>

char *ft_strcpy(char *dest, char *src);

int main()
{
    char src[] = "54321", dest[8] = "aaaaaaaa";
    printf("Expected: %s", strcpy(dest, src));
    char src2[] = "54321", dest2[8] = "aaaaaaaa";
    printf("Returns: %s", ft_strcpy(dest2, src2));
}
