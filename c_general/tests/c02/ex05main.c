#include <stdio.h>

int ft_str_is_uppercase(char *str);

int main()
{
    char *str = "TEST";
    printf("Expected 1, returned %d\n", ft_str_is_uppercase(str));

    str = "low3r case";
    printf("Expected 0, returned %d\n", ft_str_is_uppercase(str));

    str = "@[";
    printf("Expected 0, returned %d\n", ft_str_is_uppercase(str));

    str = "lowercase";
    printf("Expected 0, returned %d\n", ft_str_is_uppercase(str));

    str = "";
    printf("Expected 1, returned %d\n", ft_str_is_uppercase(str));
}
