#include <stdio.h>
#include <string.h>

char	*ft_strncat(char *dest, char *src, unsigned nb);

int		main()
{
	printf("START PROGRAM OUTPUT\n\n");

	printf("First Test Case\n");
	char	*src = " w,orl.d";
	char	dest[100] = "Hello";
	printf("Your return: %s\n", ft_strncat(dest, src, 4));
	printf("Testing null: ");
	for (int i = 0; i < 10; i++)	
		 printf("%c", dest[i]);
	printf("\n");
	
	char	*src2 = " w,orl.d";
	char	dest2[100] = "Hello";
	printf("Intended return: %s\n", strncat(dest2, src2, 4));
	printf("Intended null end: ");
		for (int j = 0; j < 10; j++)	
		 printf("%c", dest2[j]);
	printf("\n\n");
	
	printf("Second Test Case\n");
	char	*src5 = " w,orl.d";
	char	dest5[100] = "Hello";
	printf("Your return: %s\n", ft_strncat(dest5, src5, 10));
	printf("Testing null: ");
		for (int a = 0; a < 16; a++)	
		 printf("%c", dest5[a]);
	printf("\n");
	
	char	*src6 = " w,orl.d";
	char	dest6[100] = "Hello";
	printf("Intended return: %s\n", strncat(dest6, src6, 10));
	printf("Intended null end: ");
		for (int b = 0; b < 16; b++)	
		 printf("%c", dest6[b]);
	printf("\n\n");

	printf("END PROGRAM OUTPUT\n\n");
}
