#include <unistd.h>

void    ft_sort_int_tab(int *tab, int size);

void    ft_print_number(int nb, int n_lead_zero)
{
    long    l_nb;
    int     nb_p1;
    int     nb_p2;
    char    ch;

l_nb = nb;
    if (l_nb < 0)
    {
write(1, "-", 1);
l_nb = l_nb * -1;
    }
    nb_p1 = l_nb / 10;
    nb_p2 = l_nb % 10;
    if (nb_p1 > 0 || n_lead_zero > 1)
    {
ft_print_number(nb_p1, --n_lead_zero);
    }
    ch = '0' + nb_p2;
write(1, &ch, 1);
} 

void    ft_print_tab(int *p_tab, int size)
{
    int i;

i = 0;
    while (i < size)
    {
if (i > 0)
write(1, ", ", 2);
ft_print_number(p_tab[i], 0);
++i;
    }
}

void    ft_init_tab(int *p_tab)
{
    p_tab[0] = 56;
    p_tab[1] = 35;
    p_tab[2] = 24;
    p_tab[3] = 86;
    p_tab[4] = 98;
    p_tab[5] = 2;
    p_tab[6] = 42;
}

int     main(void)
{
    int a_tab[7];

ft_init_tab(a_tab);
    write(1, "Original:   ", 11);
    ft_print_tab(a_tab, sizeof(a_tab) / sizeof(int));
    write(1, "\n", 1);
    ft_sort_int_tab(a_tab, sizeof(a_tab) / sizeof(int));
    write(1, "Sorted_Asc: ", 11);
    ft_print_tab(a_tab, sizeof(a_tab) / sizeof(int));
    write(1, "\n", 1);
    return (0);
}
