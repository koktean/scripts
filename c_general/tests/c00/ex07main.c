#include <stdio.h>

void ft_putnbr(int n);

int main()
{
    ft_putnbr(-2147483648); 
    ft_putnbr(-12345671); 
    ft_putnbr(-4); 
    ft_putnbr(0); 
    ft_putnbr(9); 
    ft_putnbr(10); 
    ft_putnbr(12345678); 
    ft_putnbr(214743647);
}
